#!/bin/bash
echo deploy configs
export BUPD=$(mktemp -d --tmpdir "config-backup-XXXXXX")
export BUP_USED="${BUPD}/__bup_used"
export CHANGED="${BUPD}/__changed"
export MISSING_CMD="${BUPD}/__missing_cmd"

export EX_CLONE=11
export EX_MKDIR=12

export CLR_ERROR="\033[1;31m"
export CLR_ALREADY="\033[0;32m"
export CLR_NONE="\033[00m"

export MYBIN="$(dirname "$(dirname "$(readlink -f "$0")")")/_bin"
export PATH="$MYBIN:${PATH}"
deplink() {
    "$MYBIN/deplink" "$@"
}
mkdir -p "$HOME/.termux/tasker"
cp ./fosh-cleanup.sh "$HOME/.termux/tasker/fosh-cleanup.sh"

