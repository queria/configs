#!/bin/bash
set -ex
command -v git &> /dev/null || pkg install git

[[ -d ~/configs ]] || git clone git@gitlab.com:queria/configs.git ~/configs
cd ~/configs/termux
./deploy.sh
