#!/bin/bash

set -e

while [[ "$#" -gt 0 ]]; do
    case "$1" in
        "-v")
            export VERBOSE=1
            ;;
        "test")
            TEST=1
            ;;
    esac
    shift
done

if [[ "${VERBOSE:-0}" == "1" ]]; then
    set -x
fi

storage="$HOME/storage/shared/Documents/fallout_shelter_backup"

function sum_dir {
    local BUP="$1"
    local contentSum=""
    if [[ -f "$BUP/qsSum" ]]; then
        contentSum=$(cat "$BUP/qsSum")
    fi
    if [[ -z "$contentSum" ]]; then
        contentSum=$(find "$BUP" -type f \! -name qsSum -print0 | sort -z -h | xargs -0 -r -n1 cat | sha1sum | awk '{ print $1 }')
        cat > "$BUP/qsSum" <<<"$contentSum"
    fi
    echo "$contentSum"
}

function process_all {
    cd "${storage}"
    backups="$(find . -maxdepth 1 -type d | sort -h | tail -n +2)"
    ( IFS=$'\n';
    prevSum="";
    for BUP in $backups; do
        # echo "==$BUP==";
        contentSum=$(sum_dir "$BUP")
        if [[ -n "$prevSum" && "$prevSum" == "$contentSum" ]]; then
            echo " * deleting duplicit content ${BUP}"
            rm -rf "$BUP"
        else
            prevSum="$contentSum"
        fi
    done )
    #echo "foshbackup @ $(date)" >> ~/foshb.log
}

function pretest {
    echo "===== TEST SETUP ===="
    mkdir -p "${storage}"
    f1=$(xxd -l 64 /dev/urandom)
    f2=$(xxd -l 64 /dev/urandom)

    function fakeEntry {
        local stamp="$1"
        local cont="$2"

        echo "Faking dir entry $stamp/$(sha1sum <<<"$cont"|awk '{ print $1 }')"

        mkdir -p "${storage}/$stamp"
        rm -f "${storage}/$stamp/qsSum"
        cat > "${storage}/$stamp/fileA" <<<"$cont"
    }

    # dirs 11 and 22 are same, 33 is 'new/different', expecation is 22 gets deleted
    stamp1="2023-08-01 01.11.11"
    stamp2="2023-08-01 01.22.22"
    stamp3="2023-08-01 01.33.33"

    fakeEntry "$stamp1" "$f1"
    fakeEntry "$stamp2" "$f1"
    fakeEntry "$stamp3" "$f2"

    echo "===== TEST RUN ===="
    (set +x; "$0")

    echo "===== TEST VERIFICATION ===="
    if [[ ! -d "$storage/$stamp1" ]]; then
        echo "ERROR: missing $stamp1"
    else
        echo "OK: $stamp1/$(cat "${storage}/$stamp1/qsSum")"
    fi
    if [[ -d "$storage/$stamp2" ]]; then
        echo "ERROR: existing $stamp2"
    else
        echo "OK: $stamp2/deleted"
    fi
    if [[ ! -d "$storage/$stamp3" ]]; then
        echo "ERROR: missing $stamp3"
    else
        echo "OK: $stamp3/$(cat "${storage}/$stamp3/qsSum")"
    fi
}

if [[ "${TEST:-0}" == "1" ]]; then
    pretest
else
    process_all
fi
