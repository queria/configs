#!/usr/bin/bash

function disable_sleep_on_lid {
    if [[ ! -f /etc/systemd/logind.conf.d/dont-sleep.conf ]]; then
    	sudo mkdir -p /etc/systemd/logind.conf.d/
    	sudo cp -v ./logind-dont-sleep.conf /etc/systemd/logind.conf.d/dont-sleep.conf
    fi
}

function polkit_rules {
    # nmcli does not work well on f31 anymore complaining about me not authorized (sudo works though, right groups anyway hmm)
    sudo cp polkit-nmcli.pkla /etc/polkit-1/localauthority/50-local.d/allow-wheel-networking.pkla
    # as also flatpak thinks for whatever reason that me over ssh is not me
    sudo cp polkit-flatpak.rules /etc/polkit-1/rules.d/60-flatpak.rules

    sudo systemctl reload polkit
}

function disable_pagers {
    sudo cp unfuck-systemd-pager.sh /etc/profile.d/
    sudo cp unfuck-systemd-pager.csh /etc/profile.d/
}

cd "$(dirname "$(readlink -f "$0")")"

if [[ -f /run/ostree-booted ]]; then

    echo "Seems this is ostree based OS, skipping usual 'dnf install' and using atomic/system.sh instead"
    ./atomic/system.sh init
    disable_sleep_on_lid
    disable_pagers
    polkit_rules

    exit 0
fi

disable_sleep_on_lid
disable_pagers

PKGS=""
PKGS="$PKGS gcc golang redhat-rpm-config"
PKGS="$PKGS python3-devel python3-pip"
PKGS="$PKGS strace gdb"
PKGS="$PKGS screen tmux"
PKGS="$PKGS htop iftop iptraf-ng mc inotify-tools"
PKGS="$PKGS zsh autojump-zsh"
PKGS="$PKGS zstd p7zip p7zip-plugins"
PKGS="$PKGS git vim-enhanced neovim"
PKGS="$PKGS ncdu enca elinks kbd"
#PKGS="$PKGS rust cargo cmake freetype-devel fontconfig-devel" # deps fro alacritty


read -t 10 -p "Also install graphical user tools (gvim, i3, gimp etc)? [y/N] " XTOOLS
XTOOLS="${XTOOLS:-no}"
if [[ "$XTOOLS" == "y" || "$XTOOLS" == "yes" ]]; then
    PKGS="$PKGS i3 feh fluxbox i3lock oxygen-icon-theme lxqt-themes dunst"
    PKGS="$PKGS rxvt-unicode"
    PKGS="$PKGS xfce4-terminal xfce4-clipman-plugin"
    PKGS="$PKGS network-manager-applet xosd xclip xbacklight qt5-qttools"
    PKGS="$PKGS geeqie webp-pixbuf-loader xcf-pixbuf-loader"
    PKGS="$PKGS scrot gimp gwenview kcolorchooser"
    PKGS="$PKGS gvim mousepad pcmanfm-qt"
    PKGS="$PKGS python3-flake8 python3-jedi"
    PKGS="$PKGS gstreamer-ffmpeg mpv ffmpeg konversation pavucontrol-qt okular"
    # for bluejeans and similar media thingies
    PKGS="$PKGS gstreamer1-libav gstreamer1-vaapi"
    PKGS="$PKGS $(echo gstreamer1-plugins-{good,good-extras,ugly,bad-free,bad-freeworld})"
    # for configuring themes used by Qt (icons ...)
    PKGS="$PKGS qt5ct"
fi

set -e
sudo dnf install $PKGS
set +e

pipInstallBin() {
    local bin="$1"
    local pkg="${2:-${bin}}"

    if [[ ! -f "$HOME/.local/bin/${bin}" ]]; then
        pip3 install -U --user "${pkg}";
    else
        echo "pip: $bin already present"
    fi
}
pipInstallBin virtualenv
pipInstallBin ipython
pipInstallBin openstack python-openstackclient
pipInstallBin subliminal

if [[ ! -f /usr/local/bin/dmenu_hist ]]; then
    export GOPATH=$HOME/all/src/go
    mkdir -p $GOPATH
    go get gitlab.com/queria/dmenu_hist
    sudo cp $GOPATH/bin/dmenu_hist /usr/local/bin/
else
    echo "dmenu_hist already present"
fi

if [[ -f /usr/sbin/x2gocleansessions ]]; then
    sudo sed 's/while(sleep 2)/my $once=1; while($once--)/' -i /usr/sbin/x2gocleansessions
    SYSD=/etc/systemd/system/
    if [[ ! -f "$SYSD/x2gocleansessions.service" ]]; then
        sudo systemctl stop x2gocleansessions.service
        sudo systemctl disable x2gocleansessions.service
        sudo cp ./x2gocleansessions.service $SYSD/
        sudo cp ./x2gocleansessions.timer $SYSD/
        sudo systemctl daemon-reload
        sudo systemctl enable x2gocleansessions.timer
        sudo systemctl start x2gocleansessions.timer
    fi
fi

#### alacritty is disabled for now, urxvt(256c daemon-clients) seem very mouch enough
#### for me while being more lightweight in all ways (deps, resource usage [gpu=>heating ... for term ugh!])
# if [[ ! -f /usr/local/bin/alacritty ]]; then
#     cargo install --git https://github.com/jwilm/alacritty
#     sudo cp -v $HOME/.cargo/bin/alacritty /usr/local/bin/alacritty
# else
#     echo "alacritty already present"
# fi

# authselect crap is making this too complicated, i'm not interested so screw it
if ! grep -q ' delay=500000$' /etc/pam.d/password-auth; then
    sudo sed -ri 's/ delay=(.*)$/ delay=500000/' /etc/pam.d/password-auth
fi
# AS_PAM_CFG=/etc/authselect/password-auth
# if [[ -f "$AS_PAM_CFG" ]]; then
#     WANTED_PAMDELAY=500000
#     if grep -q 'faildelay.so delay=2000000' "$AS_PAM_CFG"; then
#         sudo sed -ri "s/(faildelay.so delay=)(.*)$/\1${WANTED_PAMDELAY}/" "$AS_PAM_CFG";
#         sudo authselect apply-changes
#         ## WRONG APPROACH - DOES NOT WORK
#         ## seems i'm expected to create new symlink based profile, replace/modify the config i want
#         ## and then switch to new profile, and then maintain it myself ... seriously, why not just maintain it myself without all this?
#     fi
# fi

polkit_rules

FONTS="$HOME/.local/share/fonts"
[[ -d "$FONTS" ]] || mkdir -p "$FONTS"
cd "$FONTS"
if [[ ! -f $FONTS/Hasklig-Regular.otf ]]; then
    curl -L --fail 'https://github.com/i-tu/Hasklig/releases/download/1.1/Hasklig-1.1.zip' -O
    7z x Hasklig-1.1.zip
    rm -f Hasklig-1.1.zip
fi
#if [[ ! -f $FONTS/Monaco-Linux.ttf ]]; then
#    curl -L --fail 'https://github.com/hbin/top-programming-fonts/raw/master/Monaco-Linux.ttf' -o "$FONTS/Monaco-Linux.ttf"
#else
#    echo "Monaco font already present"
#fi
#if [[ ! -f $FONTS/CascadiaCode.ttf ]]; then
#    CascV=2005.15
#    curl -L --fail 'https://github.com/microsoft/cascadia-code/releases/download/v${CascV}/CascadiaCode_${CascV}.zip' -O
#    7z x "CascadiaCode_${CascV}.zip" "ttf/*"
#    mv ttf/*.ttf ./
#    rm -rf ttf
#    rm -f CascadiaCode_${CascV}.zip
#fi
set -x
sudo fc-cache -f  # really needed with sudo?
fc-cache -f
set +x

[[ "$SHELL" = "/bin/zsh" ]] || chsh -s /bin/zsh
