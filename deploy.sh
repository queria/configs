#!/bin/bash

set -eo pipefail

# set  to 'y' to get anonymous git clones
# (e.g. when used in vm/machine without git ssh keys)
ANONGIT="${ANONGIT:-no}"

export BUPD=$(mktemp -d --tmpdir "config-backup-XXXXXX")
export BUP_USED="${BUPD}/__bup_used"
export CHANGED="${BUPD}/__changed"
export MISSING_CMD="${BUPD}/__missing_cmd"

export EX_CLONE=11
export EX_MKDIR=12

export CLR_ERROR="\033[1;31m"
export CLR_ALREADY="\033[0;32m"
export CLR_NONE="\033[00m"

export MYBIN="$(dirname "$(readlink -f "$0")")/_bin"
export PATH="$MYBIN:${PATH}"

safetycheck() {
    if [[ "$1" != "--force" && "$HOME" != "$(echo ~)" ]]; then
        echo "$$HOME[=$HOME] does not seem to match ~[=$(echo ~)] ... operation could be unsafe!"
        echo "You can use --force to override this check."
        exit 1
    fi
    if [[ -z "$BUPD" ]]; then
        echo "Issue with getting temporary backup dir!"
        exit 1
    fi
}
safemkdir() {
    if ! mkdir -vp "$1"; then
        fin_message "mkdir $1"
        exit $EX_MKDIR
    fi
}
deprepo() {
    if ! "$MYBIN/deprepo" "$@"; then
        fin_message "Cloning $repo into $tgt"
        exit $EX_CLONE
    fi
}
deplink() {
    "$MYBIN/deplink" "$@"
}
sudo_deplink() {
    SUDO="sudo -E" "$MYBIN/deplink" "$@"
}

cmd_check() {
    if ! eval $1 &> /dev/null; then
        touch "$MISSING_CMD"
        echo "Missing $2. $3";
    fi
}
bin_check() { cmd_check "which $1" "command $1" "$2"; }

deploy_repos() {
    if [[ ! -d "$HOME/all/src" ]]; then
        touch "$CHANGED"
        mkdir -vp "$HOME/all/src"
    fi
    if [[ "y" = $ANONGIT ]]; then
      deprepo https://gitlab.com/queria/scripts "$HOME/all/src/scripts"
      deprepo https://gitlab.com/queria/my-vim "$HOME/.vim" "submodules"
    else
      deprepo git@gitlab.com:queria/scripts.git "$HOME/all/src/scripts"
      # deprepo git@gitlab.com:queria/st.git "$HOME/all/src/st"
      deprepo git@gitlab.com:queria/my-vim.git "$HOME/.vim" "submodules"
    fi
    deprepo https://github.com/robbyrussell/oh-my-zsh.git "$HOME/.oh-my-zsh"
    deprepo https://github.com/tarruda/zsh-autosuggestions.git "$HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions"
    deprepo https://github.com/zsh-users/zsh-syntax-highlighting.git "$HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting"
    deprepo https://github.com/agiz/youtube-mpv.git "$HOME/all/src/youtube-mpv"
    if [[ "y" = $ANONGIT ]]; then
        deprepo https://gitlab.com/queria/ohmyzsh-bgnotify-tmux.git "$HOME/.oh-my-zsh/custom/plugins/bgnotify"
    else
        deprepo git@gitlab.com:queria/ohmyzsh-bgnotify-tmux.git "$HOME/.oh-my-zsh/custom/plugins/bgnotify"
    fi
}
deploy_configs() {
    # vim/neovim
    deplink "$HOME/.vim/qs_vimrc" "$HOME/.vimrc"
    safemkdir "$HOME/.config/nvim"
    deplink "$HOME/.vim/nvim_init.vim" "$HOME/.config/nvim/init.vim"

    # shells

    deplink .bash_profile "$HOME/.bash_profile"
    deplink .bashrc "$HOME/.bashrc"
    deplink .bash_aliases "$HOME/.bash_aliases"
    deplink .bashnorc "$HOME/.bashnorc"
    deplink .bc "$HOME/.bc"

    deplink .zshrc "$HOME/.zshrc"
    deplink .zsh_completion "$HOME/.zsh_completion"
    deplink .zsh_bashcomp.d/ "$HOME/.zsh_bashcomp.d"
    deplink .zshenv "$HOME/.zshenv"
    deplink .zlogout "$HOME/.zlogout"

    # usability tools

    deplink tmux.conf "$HOME/.tmux.conf"

    # dev/build tools

    deplink .gitattributes "$HOME/.gitattributes"
    deplink .gitconfig "$HOME/.gitconfig"
    deplink .gitexcludes "$HOME/.gitexcludes"

    safemkdir "$HOME/.cargo"
    deplink cargo_config.toml "$HOME/.cargo/config.toml"



####### TEMP DISABLED during rewrite
###     safemkdir "$HOME/.config/fish"
###     deplink config.fish "$HOME/.config/fish/config.fish"
###
###     safemkdir "$HOME/.config"
###     safemkdir "$HOME/.local/share/applications"
###     deplink mimeapps.list "$HOME/.config/mimeapps.list"
###     deplink mimeapps.list "$HOME/.local/share/mimeapps.list"
###     deplink mimeapps.list "$HOME/.local/share/applications/mimeapps.list"
###     deplink browser.desktop "$HOME/.local/share/applications/browser.desktop"
###
###     deplink .Xdefaults "$HOME/.Xdefaults"
###
###     safemkdir "$HOME/.i3"
###     gen i3-config.sh "$HOME/.i3/config"
###     deplink i3-status "$HOME/.i3status.conf"
###     safemkdir "$HOME/.config/dunst"
###     deplink dunstrc "$HOME/.config/dunst/dunstrc"
###     deplink addic7ed "$HOME/.config/addic7ed"
###     safemkdir "$HOME/.config/openbox"
###     deplink openbox.rc.xml "$HOME/.config/openbox/rc.xml"
###     deplink openbox.rc.xml "$HOME/.config/openbox/lxqt-rc.xml"
###
###     safemkdir "$HOME/.mplayer"
###     deplink mplayer "$HOME/.mplayer/config"
###     safemkdir "$HOME/.mpv"
###     deplink mpv "$HOME/.mpv/config"
###
###     safemkdir "$HOME/.xkb/symbols"
###     deplink vok "$HOME/.xkb/symbols/vok"
###
###     #safemkdir "$HOME/.config/alacritty"
###     #deplink alacritty.yml "$HOME/.config/alacritty/alacritty.yml"
###
###     safemkdir "$HOME/bin"
###     deplink "/usr/bin/openstack" "$HOME/bin/os"
###
###     sudo_deplink vok "/usr/share/X11/xkb/symbols/vok"
###
###     safemkdir "$HOME/.urxvt/ext"
###     if [[ ! -f "$HOME/.urxvt/ext/resize-font" ]]; then
###          curl 'https://raw.githubusercontent.com/simmel/urxvt-resize-font/master/resize-font' -o $HOME/.urxvt/ext/resize-font
###     fi
###
###     dconf_load /org/gnome/terminal/ ./gnome-terminal.dconf
###
###     safemkdir "$HOME/.config/xfce4/terminal"
###     deplink xfce4-terminal-accels.scm "$HOME/.config/xfce4/terminal/accels.scm"
###     deplink xfce4-terminal-terminalrc "$HOME/.config/xfce4/terminal/terminalrc"
###     safemkdir "$HOME/.config/xfce4/terminal/colorschemes"
###     deplink xfce4-terminal-colorschemes/qsDark.theme "$HOME/.config/xfce4/terminal/colorschemes/qsDark.theme"
###     deplink xfce4-terminal-colorschemes/qsLight.theme "$HOME/.config/xfce4/terminal/colorschemes/qsLight.theme"
###
###     safemkdir "$HOME/.config/xfce4/xfconf/xfce-perchannel-xml"
###     deplink xfce4-panel.xml "$HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml"
###
###     safemkdir "$HOME/.local/share/konsole/"
###     deplink konsole-qs1.colorscheme "$HOME/.local/share/konsole/qs1.colorscheme"
###     deplink konsole-qsLight.colorscheme "$HOME/.local/share/konsole/qs1.colorscheme"
}
####### TEMP DISABLED during rewrite
### deploy_apps() {
###     # if [[ ! -f "$HOME/bin/st" ]]; then
###     #     echo "$HOME/bin/st: compile && install ..."
###     #     set -ex
###     #     pushd "$HOME/all/src/st" > /dev/null
###     #     make
###     #     cp "$PWD/st" "$HOME/bin/st"
###     #     popd > /dev/null
###     #     set +ex
###     # else
###     #     echo -e "${CLR_ALREADY}$HOME/bin/st [already]${CLR_NONE}";
###     # fi
###     echo "st terminal not used lately, skipping"
### }
####### TEMP DISABLED during rewrite
### check_app_presence() {
###     bin_check vim
###     bin_check gvim
###     bin_check htop
###     bin_check ncdu
###     bin_check screen
###     bin_check tmux
###     bin_check strace
###     bin_check zsh
###     bin_check autojump
###     bin_check fortune
###     bin_check enca
###     bin_check ctags
###     bin_check elinks
###     bin_check vncviewer
###     bin_check osd_cat
###     bin_check pip
###     bin_check virtualenv
###     bin_check flake8 "python{,3}-flake8"
###     bin_check openstack "python-openstackclient"
###     bin_check scrot
###     bin_check pcmanfm-qt
###     bin_check geeqie
###     bin_check mousepad
###     cmd_check 'python -c "import jedi"' "python{,3}-jedi"
###     cmd_check '[[ "$SHELL" =~ .*zsh ]]' "zsh as user shell"
### }


safetycheck "$1"

cd "$(dirname $0)"
deploy_repos
deploy_configs
./atomic/flatpaks.sh
./atomic/toolboxes.sh
####### TEMP DISABLED during rewrite
### deploy_apps
###
### check_app_presence
###
### if [[ ! -f "${MISSING_CMD}" ]]; then
###     if ! ./youtube-mpv-install.sh "$HOME/all/src/youtube-mpv"; then
###         fin_message "youtube-mpv setup, please debug it:\n  ./youtube-mpv-install.sh '$HOME/all/src/youtube-mpv'"
###     fi
### fi
### systemctl enable --user pulseaudio
### systemctl start --user pulseaudio

fin_message
