#!/bin/bash
# vim: set et sw=4 ts=4 ft=sh:

set -eo pipefail

if [[ "$1" == "--help" ]]; then
    echo "$0 [--update]"
    echo ""
    exit
fi
UPDATE=no
[[ "$1" != "--update" ]] || { shift; UPDATE=update; }

FVER=${FVER:-$(sed -nr 's/^VERSION_ID=//p' /etc/os-release)}
CNTBDIR="$(dirname "$(readlink -f "$0")")/toolboxes"

# first ensure images are built
${CNTBDIR}/build.sh $([[ "$UPDATE" == "no" ]] || echo "\--update")

declare TB_NAME
declare -a TB_ARGS
declare -a PKGS
declare -a PKGS_EVERYWHERE
declare -a BINWRAP

ensure_toolbox() {
    local image="queria-toolbox:${FVER}-${TB_NAME}"
    local image_latest_hash=""
    local image_used_hash=""
    image_used_hash=$(podman inspect "${TB_NAME}" | jq -r '.[0].ImageDigest // empty' || true)
    image_latest_hash=$(podman image inspect "${image}" | jq -r '.[0].Digest')

    # new image available, remove toolbox container (to later recreate)
    if [[ -n "${image_used_hash}" && "${image_used_hash}" != "${image_latest_hash}" ]]; then
        image_used_hash=""
        podman stop "${TB_NAME}"
        podman rm "${TB_NAME}"
    fi

    # create container if missing
    if [[ -z "${image_used_hash}" ]]; then
        toolbox create -i ${image} ${TB_ARGS[@]} $TB_NAME
    fi

    # if defined call post-creation hook
    if [[ "$(type -t "ensure_toolbox_${TB_NAME}")" == function ]]; then
        ensure_toolbox_${TB_NAME}
    fi
    # install extra packages into running container
    if [[ -n "${PKGS_EVERYWHERE[*]}" || -n "${PKGS}" ]]; then
        toolbox run -c $TB_NAME \
            bash -c "set -eo pipefail; miss=\$(rpm -q ${PKGS_EVERYWHERE[*]} ${PKGS[*]} | sed -nr 's/package (.*) is not installed/\\1/p'); [[ -z \"\$miss\" ]] || (set -x; sudo dnf install -y \$miss)"
    fi
    # update running container
    if [[ "update" == "$UPDATE" ]]; then
        toolbox run -c $TB_NAME sudo dnf update -y
    fi
    regen_links
    echo "Toolbox ${TB_NAME} DONE"
}

regen_links() {
    # file path of bin-wrapper shell script
    local wrapper
    # list of bin entries to link to bin-wrapper
    local binaries
    # for each binary explicitly installed, create helper wrapper in ~/bin/
    # - clean wrapper links for current toolbox (ensures no removed package links left)
    mkdir -p "$HOME/bin"
    wrapper="$HOME/bin/_tb_wrapper_${TB_NAME}.sh"
    cat > $wrapper <<EOF
#!/bin/bash
bn=\$(basename "\$0")
if [[ -x /usr/bin/\$bn ]]; then
  exec /usr/bin/\$bn "\$@"
else
  toolbox run -c "$TB_NAME" "/usr/bin/\$bn" "\$@"
fi
EOF
    chmod +x "$wrapper"

    find "$HOME/bin/" -type l -lname "$wrapper" -delete

    binaries="${BINWRAP[*]}"
    binaries="${binaries} $(podman inspect "${TB_NAME}" | jq -r '(.[0].Config.Labels | to_entries | map(select(.key | contains("queria.toolbox.binwrap") ))) | map(.key | split(".") | last)[]')"
    for b in $binaries; do
        b=$(basename $b);
        if [[ ! -e "$HOME/bin/$b" || -h "$HOME/bin/$b" ]]; then
            ln -snf "${wrapper}" "$HOME/bin/$b"
        else
            echo "WARNING: Unable to use existing $HOME/bin/$b for invoking $b from $TB_NAME toolbox" >&2
        fi
    done
}

clean_missing_links() {
    # post install cleanup to remove any wrappers which are missing containers
    # and then links to missing wrappers
    local tb_name=""
    local wrap_name=""
    echo "Cleaning binwrappers for missing containers"
    while read wrap_name; do
        tb_name="${wrap_name#_tb_wrapper_}"
        tb_name="${tb_name%.sh}"
        ! podman container exists "${tb_name}" || continue
        rm -vf "$HOME/bin/${wrap_name}"
    done < <(find "$HOME/bin/" -name _tb_wrapper_\* -printf '%f\n')
    echo "Cleaning broken binwrap symlinks"
    find -L "$HOME/bin" -lname "$HOME/bin/_tb_wrapper_*" -print -delete
}
# here i want to record recipes for all the 'permanent' toolbox(es) content i use
# the approach of transfering list of installed rpms between versions did not worked that well

# some packages i want always everywhere
# clearly better would be to use custom images with layers on top of fedora-toolbox
#PKGS_EVERYWHERE=(neovim jq make p7zip zstd)


TB_NAME="fedora-toolbox-$FVER"
TB_ARGS=()
BINWRAP=()
PKGS=()
## PKGS+=(cmake)
## PKGS+=(colordiff)
## BINWRAP+=(colordiff)

ensure_toolbox

# for building qt based apps
TB_NAME="qtdevel"
TB_ARGS=()
BINWRAP=()
PKGS=()
ensure_toolbox

TB_NAME="rust"
TB_ARGS=()
BINWRAP=()
PKGS=()
## ensure_toolbox_rust() {
##     toolbox run -c rust bash -c \
##         "rpm -q cosign || sudo dnf install -y https://github.com/sigstore/cosign/releases/download/v2.4.1/cosign-2.4.1-1.x86_64.rpm"
## }
ensure_toolbox

####
clean_missing_links

echo "== Toolboxes: All done =="
