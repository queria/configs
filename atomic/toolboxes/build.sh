#!/bin/bash

source <(grep ^VERSION_ID /etc/os-release)
FVER=${FVER:-$(sed -nr 's/^VERSION_ID=//p' /etc/os-release)}
CNTBDIR="${CNTBDIR:-$(dirname "$(readlink -f "$0")")}"

DNFCACHE=""
# comment out to disable local chache dir
mkdir -p "${CNTBDIR}/_dnf_cache"
DNFCACHE="-v ${CNTBDIR}/_dnf_cache:/var/cache/libdnf5:z"

UPDATE="no"

function build_img {
    local imgname="$1"
    local tag="${2:-queria-toolbox:${FVER}-${imgname}}"

    local prevdir=${PWD}
    cd "$CNTBDIR"

    local cfile_hash="0"
    cfile_hash=$( (echo "$FVER"; cat Containerfile.${imgname}) | sha1sum | cut -f1 -d' ')

    local existing_hash=""
    existing_hash=$(podman inspect localhost/${tag} 2>/dev/null | jq -r '.[0].Labels["queria.toolbox.cfile_hash"] // empty' || true)

    local res=0
    if [[ "update" == "$UPDATE" || "${existing_hash}" != "${cfile_hash}" ]]; then
        podman build \
            --omit-history \
            --tag ${tag} \
            --build-arg FED_VER=${FVER} \
            --build-arg CFILE_HASH=${cfile_hash} \
            ${DNFCACHE} --file Containerfile.${imgname} . || true
        res=${PIPESTATUS[0]}
    fi
    cd "${prevdir}"
    return ${res}
}

if [[ "$1" == "--help" ]]; then
    echo "$0 [--update] [imagename [imagetag]]"
    echo ""
    echo "Build my base image and images for my toolboxes: fedora-toolbox/qtdevel/rust toolboxes"
    echo ""
    echo "Optionally specify parameters for build_img function"
    echo ""
    echo "--update skips comparison of Containerfile checksum (does not skip podmans cached layers)"
    exit 0
fi

if [[ "$1" == "--update" ]]; then
    UPDATE=update
    shift
fi

if [[ -n "$1" ]]; then
    build_img "$@"
else
    build_img base
    pids=""
    build_img fedora-toolbox queria-toolbox:${FVER}-fedora-toolbox-${FVER} &
    pids="$pids $!"
    build_img qtdevel &
    pids="$pids $!"
    build_img rust &
    pids="$pids $!"
    wait $pids
fi

