#!/bin/bash
# vim: set et sw=4 ts=4 ft=sh:

FVER=$(source /etc/os-release; echo $VERSION_ID)

function has_nvidia_gpu { lspci -d ::0300 | grep -q -i nvidia; }
function has_intel_gpu { lspci -d ::0300 | grep -q -i nvidia; }

# old notes - for upgrading to F40 do:
## rpm-ostree rebase fedora:fedora/40/x86_64/kinoite --uninstall mozilla-openh264
# then after upgrade:
## rpm-ostree override remove noopenh264 --install openh264 --install mozilla-openh264

# as media related packages almost always conflicts on Fedora, blocking udpate,
# keep track of them separately for easier override/reset/overlay actions
# support a/v content
MED_REMOVE=""
MED_REMOVE="$MED_REMOVE libavfilter-free"
MED_REMOVE="$MED_REMOVE libavformat-free"
MED_REMOVE="$MED_REMOVE libpostproc-free"
MED_REMOVE="$MED_REMOVE libswresample-free"
MED_REMOVE="$MED_REMOVE libavutil-free"
MED_REMOVE="$MED_REMOVE libavdevice-free"
MED_REMOVE="$MED_REMOVE ffmpeg-free"
MED_REMOVE="$MED_REMOVE libavcodec-free"
MED_REMOVE="$MED_REMOVE libswscale-free"
if [[ $FVER -ge 40 ]]; then
    MED_REMOVE="$MED_REMOVE noopenh264"
fi

MED_ADD=""
# support a/v content
MED_ADD="$MED_ADD libva-utils"
MED_ADD="$MED_ADD ffmpeg"
MED_ADD="$MED_ADD mozilla-openh264"
MED_ADD="$MED_ADD openh264"


# Then list of any other extra packages

REMOVE=""
# REMOVE="$REMOVE "

ADD=""
# always ensure there is rpmfusion
# (useful to keep here in addition to separate handling,
#  in situation rebasing with 'reset' to different refs and back
#  (like bazzite or silveblue/kinoite etc)
#  as repos may be kept in place when booting the other deployment,
#  while package layering may be dropped while resetting)
ADD="$ADD rpmfusion-free-release rpmfusion-nonfree-release"
# drivers
if has_intel_gpu; then
    ADD="$ADD libva-intel-driver"
    ADD="$ADD intel-media-driver"
fi
if has_nvidia_gpu; then
    ADD="$ADD kmod-nvidia"
    # FIXME: retest and drop
    ## xorg was needed instead of kde/plasma with wayland
    ## but with kde 6.1 + nvidia 555.58 ... waylaynd seems works for gaming?
    ## but, as there may be few issues, keep it installed
    ADD="$ADD xorg-x11-drv-nvidia plasma-workspace-x11"
fi
# shell
ADD="$ADD zsh"
ADD="$ADD autojump-zsh"
# useful admin tools
ADD="$ADD etckeeper"
ADD="$ADD git"
ADD="$ADD git-extras"
ADD="$ADD git-filter-repo"
ADD="$ADD htop"
ADD="$ADD iftop"
ADD="$ADD iptraf-ng"
ADD="$ADD lm_sensors"
ADD="$ADD mc"
ADD="$ADD ncdu"
ADD="$ADD p7zip"
ADD="$ADD screen"
ADD="$ADD strace"
ADD="$ADD tcpdump"
ADD="$ADD tmux"
ADD="$ADD zstd"
# other user extras (consider toolbox instead)
ADD="$ADD elinks"
ADD="$ADD enca"
ADD="$ADD flatpak-builder"
ADD="$ADD gcc"
#ADD="$ADD gimp"
#ADD="$ADD git-review"
#ADD="$ADD golang"
ADD="$ADD guestfs-tools"
ADD="$ADD inotify-tools"
ADD="$ADD kdb"
#ADD="$ADD konversation"
ADD="$ADD krb5-workstation"
ADD="$ADD neovim"
ADD="$ADD python3-devel"
#ADD="$ADD python3-flake8"
#ADD="$ADD python3-jedi"
ADD="$ADD python3-pip"
ADD="$ADD pv"
if [[ "$FVER" == 39 ]]; then
    # seems repo content is broken (wrong version build '-4' instead of '-3' exists, dnf fails on http 404)
    # curl -L -O 'http://ftp-stud.hs-esslingen.de/pub/Mirrors/rpmfusion.org/nonfree/fedora/updates/39/x86_64/s/steam-devices-1.0.0.79-4.fc39.i686.rpm'
    # rpm-ostree install ./steam-devices-1.0.0.79-4.fc39.i686.rpm
    :
else
    ADD="$ADD steam-devices"
fi
ADD="$ADD vim-enhanced"
ADD="$ADD virt-install"
ADD="$ADD virt-manager"
ADD="$ADD virt-viewer"
ADD="$ADD vagrant"

list2args() {
    local opt="$1"
    local args=""

    for item in $2; do
        args="$args --$opt=$item"
    done

    echo "$args"
}
list2substract() {
    local a="$1"
    local b="$2"
    local remaining=""

    for item in $1; do
        if ! grep -E -q "(^|\s)${item}(\s|\$)" <<<"$b"; then
            remaining="$remaining $item"
        fi
    done

    echo "$remaining"
}

list_installed() {
    rpm-ostree status --json | jq -r '.deployments[0]["requested-packages"][]'
}

media_reset() {
    local un_args=$(list2args uninstall "ffmpeg")
    (set -ex; sudo rpm-ostree override reset $un_args $MED_REMOVE;)
}

media_apply() {
    local already_installed=$(list_installed)
    local already_removed=$(rpm-ostree status --json | jq -r '.deployments[0]["requested-base-removals"][]')

    local to_add="$(list2substract "$MED_ADD" "$already_installed")"
    local to_remove="$(list2substract "$MED_REMOVE" "$already_removed")"

    local in_args=""
    if [[ -n "$to_add" ]]; then
        in_args=$(list2args install "$to_add")
        # we could skip here if to_remove is empty (uncomment above)
        # but not doing that, as generally we dont care about what is removed
        # what matters is if good stuff is added
        # ...
        # sadly, rpm-ostree is very unfriendly, no idempotency
        #   rpm-ostree override remove libavfilter-free ...  noopenh264 --install=ffmpeg
        #   => error: Override already exists for package 'libavfilter-free'
        # when some override is already in place
        if [[ -n "$to_remove" ]]; then
            (set -ex; sudo rpm-ostree override remove $to_remove $in_args;)
        elif [[ -n "$to_add" ]]; then
            # when 'remove' overrides are in place but some replacement is missing
            (set -ex; sudo rpm-ostree install $to_add;)
        fi
    else
        echo "All rpm overrides already in place."
    fi
}
extra_apply() {
    if [[ -n "$REMOVE" ]]; then
        (set -ex; sudo rpm-ostree override remove $REMOVE;)
    fi
    local already_installed=$(list_installed)
    local missing=$(list2substract "$ADD" "$already_installed")
    if [[ -n "$missing" ]]; then
        (set -ex; [[ -z "$ADD" ]] || sudo rpm-ostree install --idempotent $ADD;)
    else
        echo "Extra packages already installed."
    fi
}

ensure_rpmfusion() {
    # step1 - install versioned release
    if ! (rpm-ostree status --booted | grep -q rpmfusion-free-release); then
        if (rpm-ostree status | grep -q rpmfusion-free-release); then
            echo "Seems you just need to reboot first BEFORE going switching to non-versioned rpmfusion."
        else
            (set -x; sudo rpm-ostree install \
                https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
                https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm)
        fi
        echo "Now do:   systemctl reboot"
        return 1
    fi
    # step2 - switch to non-versioned
    if (rpm-ostree status --booted | grep -q 'LocalPackages:.*rpmfusion-free-release'); then
        (set -x; sudo rpm-ostree update \
         --uninstall rpmfusion-free-release \
         --uninstall rpmfusion-nonfree-release \
         --install rpmfusion-free-release \
         --install rpmfusion-nonfree-release)
        echo "Now do:   systemctl reboot"
        return 1
    fi
    echo "rpm-fusion already correctly installed"
}

nvidia_args() {
    if has_nvidia_gpu; then
        # if (rpm-ostree status --booted | grep kmod-nvidia); then
            if ! (rpm-ostree kargs | grep -q 'nvidia-drm'); then
                (set -x; sudo rpm-ostree kargs --append=rd.driver.blacklist=nouveau --append=modprobe.blacklist=nouveau --append=nvidia-drm.modeset=1 --append=initcall_blacklist=simpledrm_platform_driver_init)
            fi
            if ! grep -q 'nvidia-drm' /proc/cmdline; then
                echo "Now to use nvidia kernel args do:"
                echo "  systemctl reboot"
                return 1
            fi
            echo "Nvidia kernel args already applied"
        # else
        #     # UNSURE - seems it may take very long time to add kargs together with installing kmod-nvidia ... to be maybe confirmed in future 
        #     if ! (rpm-ostree status | grep kmod-nvidia); then
        #         echo "Its recommended to first install extra packages (contains kmod-nvidia) and reboot before continuing"
        #         return 1
        #     else
        #         echo "Its recommended to reboot after installing kmod-nvidia before continuing."
        #         return 1
        #     fi
        # fi
    else
        echo "no nvidia gpu, skipping kernel args"
    fi
}

flatpak_flathub() {
    if ! flatpak remotes --columns=name,priority | grep -q 'flathub\s*60'; then
        (set -ex; sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo)
        (set -ex; sudo flatpak remote-modify --system --prio=60 flathub)
    else
        echo "Flathub already configured"
    fi
}

case "x$1" in
    x--help|x) echo "$0 <init|fusion|nvidia|reset|update|extra|media>"
        ;;
    xinit)
        # init == do all steps required for system initialization
        # it may stop when reboot is needed (rpmfusion especially)
        set -e
        ensure_rpmfusion
        extra_apply
        nvidia_args
        media_apply
        flatpak_flathub
        echo "System initialization done"
        ;;
    xfusion)
        ensure_rpmfusion
        ;;
    xnvidia)
        nvidia_args
        ;;
    xreset)
        media_reset
        ;;
    xupdate)
        (set -ex; rpm-ostree upgrade;)
        ;;
    xextra)
        extra_apply
        ;;
    xmedia)
        media_apply
        ;;
esac

