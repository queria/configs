#!/bin/bash
# vim: set et sw=2 ts=2 ft=sh:

CFGDIR="$(dirname "$(readlink -f "$0")")/services"

cd /

# Possible configuration options
# (per service, set the variable in cfg_* file):
#
# PODMAN_OPTS cli args for podman (--host, volumes, service env vars)
# IMAGE       container image name
# TAG         tag (version) of container image to use
# AS_USER     run service container as specified user  (default current user)
# AS_GROUP    run service container as specified group (defaults to main group of specified user, or current user)

function load_config {
  local contName=$1

  # load per-service config overrides
  if [[ -f "${CFGDIR}/cfg_${contName}" ]]; then
    echo "Loading config for ${contName} ..."
    source "${CFGDIR}/cfg_${contName}"
  else
    echo "No config for ${contName}."
  fi

  use_sudo=""
  if [[ -n "${AS_USER}" ]]; then
    use_sudo="sudo --login -u $AS_USER"
  fi
  # TODO: move all variable/config calculations here
  # after this all needed vars should exist

  # TODO: consider either double cfg loading/evaluation or special USER handling
  # to allow referencing also calculated variables inside cfg file
  # - grep cfg for AS_USER/AS_GROUP + call ensure_user + source cfg
  #   - creates specific syntax limitation for these two variables
  # - source cfg + call ensure_user + source cfg ?
  #   - can result in weird evaluation results?
  #   - maybe do initial source cfg + ensure_user in subshell
  #   - then the last clean source cfg could not be polluted/confused by previous sourcing
  # - goal of this is to allow referencing user home dir, uid/gid and such in the cfg
  # - this still does not allow referencing container related dynamic values (id/...)
  #   but that usually should not be needed
  #   (and would need some 'actual' templating which is no-go here?)
}

function start_container {
  local contName=$1
  local tag=$2


  local opts="${PODMAN_OPTS:-}"

  local image=${IMAGE:-docker.io/gameservermanagers/gameserver}
  [[ -z "$tag" ]] && tag=${TAG:-$contName}

  # TODO: likely always pre-create $HOME/${contName}_data directory for each service
  # - even if it wouldnt be used, but most services want at least one volume
  # - and its better to avoid it being $HOME directly
  #   (as there are other files which are better not seen inside contaier)
  if [[ -n "$PRE_RUN" ]]; then
    $use_sudo bash -c "set -ex; $PRE_RUN"
  fi

  $use_sudo podman container exists $contName \
    || $use_sudo podman run -d --name $contName $opts ${image}:${tag}
}

function gen_service {
  ### quadlet/systemd containers - did not worked that well (yet), issues with Service/User, enabling and such
  # sudo cp necesse.container /etc/containers/systemd/
  # /usr/libexec/podman/quadlet -dryrun
  #
  ### also podman generate is not as a like
  # podman generate systemd necesse > ./necesse.service
  ### do not like that many hardcoded IDs and such,
  ### as i want to recreate the container on my own sometimes
  #
  ### so use custom service template where i can easily tweak it

  local contName=$1
  local svcFile="${CFGDIR}/${contName}.service"

  # [[ ! -f "$svcFile" ]] || return 0

  local userName=${AS_USER:-$USER}
  local userId=$(id -u -r ${userName})
  local groupName=${AS_GROUP:-$(id --group --name ${userName})}
  # TODO: remove the CIDFILE, instead handle it explicitely here in start_container
  # always pass `--conmon-pidfile conmon_${contName}.pid` directly, and then rely on it here
  # (or still allow CIDFILE redefining to custom path?)
  local cidFile=${CIDFILE:-$($use_sudo podman inspect --format "{{ .ConmonPidFile }}" ${contName})}

  cat > $svcFile <<EOF
[Unit]
Description=${contName} server
After=syslog.target network.target network-online.target
RequiresMountsFor=/tmp/storage-run-${userId}/containers

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
RestartSec=5s
TimeoutStopSec=70
ExecStart=/usr/bin/podman start ${contName}
ExecStop=/usr/bin/podman stop -t 20 ${contName}
ExecStopPost=/usr/bin/podman stop -t 20 ${contName}
PIDFile=${cidFile}
Type=forking
# User=${userName}
# Group=${groupName}

[Install]
#WantedBy=multi-user.target
WantedBy=default.target
EOF
  if [[ "${userName}" == "root" ]]; then
    sudo cp $svcFile /etc/systemd/system/
    sudo systemctl daemon-reload
  else
    local td="$(eval echo ~${userName})/.config/systemd/user"
    $use_sudo mkdir -p "$td"
    cat $svcFile | $use_sudo tee $td/${contName}.service
    # Now we need to stop the container so it can be started by systemd instead
    $use_sudo podman stop $contName
    $use_sudo bash -c "set -ex; export XDG_RUNTIME_DIR=/run/user/$(id -u ${userName}); systemctl --user daemon-reload; systemctl --user start ${contName}.service; systemctl --user enable ${contName}.service"
    # $use_sudo systemctl --user enable ${contName}.service
  fi
}

function gen_ctl {
  # As controlling services run under specific user's systemd instance
  # from central place (like systemwide systemd as root) is not so easy/user-friendly,
  # generate '{service}ctl' helpers for each service
  local svcName=$1
  local svcUser=$2

  local binPath="$HOME/bin/${contName}ctl"
  echo "Generating ${binPath}"
  ### FIXME: requires AS_USER, atm no support here for current or as root user
  cat > "${binPath}" <<EOF
#!/bin/bash

act="\${1:-status}";

function sudo_as_service {
  sudo --login -u ${svcUser} bash -c "\$*"
}

case "\$act" in
  --help)
    sed -nr 's/^\s*(.*)\)$/\1/p' \$0 | sed 's/^\*$/* status|start|stop|restart|any_other_systemctl_action/'
    ;;
${CTL_CASES:-}
  *)
    sudo_as_service "XDG_RUNTIME_DIR=/run/user/\\\$(id -u ${svcUser}) systemctl --user \${act} ${svcName}"
    ;;
esac
EOF
  chmod +x "${binPath}"
}

function ensure_user {
  local u=$1
  local g=$2

  if [[ -n "$u" ]] && ! id -u "$u" &> /dev/null; then
    local same_group="-U"
    if [[ -n "$g" && "$g" != "$u" ]]; then
      same_group=""
    fi
    sudo useradd --add-subids-for-system -m -r $same_group "$u"
    sudo loginctl enable-linger $u
    if [[ -n "$g" ]] && ! getent group "$g" &> /dev/null; then
      sudo groupadd -r -U "$u" "$g"
    fi
  fi
}

function prep_service {
  local contName=$1
  load_config "$contName"
  ensure_user "$AS_USER" "$AS_GROUP"
  start_container "$contName"
  gen_service "$contName"
  gen_ctl "$contName" "$AS_USER"
}

if [[ -z "$1" || "$1" == "--help" ]]; then
  echo "Usage:  $0 <service_id>"
  echo ""
  echo "where for service_id exists matching cfg_* file:"
  echo "  $(cd "${CFGDIR}"; ls -1 cfg_* | cut -c 5- | sort -h | tr '\n' ' ')"
  exit 1
fi
if [[ ! -f "${CFGDIR}/cfg_${1}" ]]; then
  echo "Config cfg_${1} not found"
  exit 1
fi
set -ex
prep_service "$1"

