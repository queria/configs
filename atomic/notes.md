# random notes from my Fedora Atomic (Kinoite) usage

## note down extra rpms installed on top of silverblue/kinoite (now in system.sh)
```
autojump-zsh elinks enca etckeeper ffmpeg flatpak-builder gcc gdb gimp git-review golang htop iftop inotify-tools intel-media-driver iptraf-ng kdb konversation krb5-workstation libva-intel-driver libva-utils mc mozilla-openh264 ncdu neovim p7zip python3-devel python3-flake8 python3-jedi python3-pip rpmfusion-free-release rpmfusion-nonfree-release screen strace tmux vim-enhanced wine zsh zstd
```

## keep some deployment for longer time (to be able to revert after multiple changes)
```
# see list of deployments
ostree admin status
# pin specific deployment
ostree admin pin 0
# unpin (free for cleaning)
ostree admin pin -u 1
```

## replace with custom locally downloaded rpm (e.g. revert to specific fixed/not-buggy kernel)
```
rpm-ostree override replace ./kernel*6.3.12*.rpm
# and to revert back to original content (drop the local override)
rpm-ostree override reset kernel kernel-core kernel-modules kernel-modules-core kernel-modules-extra
```

## major version upgrade
```
rpm-ostree rebase fedora:fedora/37/x86_64/kinoite
# possibly with --cache-only?
```

## example of replacing rpmfusion locally installed versioned with dynamically versioned
```
rpm-ostree update \
            --uninstall rpmfusion-free-release-36-1.noarch \
            --uninstall rpmfusion-nonfree-release-36-1.noarch \
            --install rpmfusion-free-release \
            --install rpmfusion-nonfree-release
```

## in scripts repository:
helpers for exporing list of installed rpms from all toolboxes (containers) with older fedora image as base
and recreating those (by reinstalling listed rpms) into new toolbox containers based on new/different fedora version
- silverblue-toolbox-reimport
- silverblue-toolbox-upgrade
